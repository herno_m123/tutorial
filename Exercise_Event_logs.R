install.packages("ggplot2")
install.packages('reshape')
install.packages("xlsx")
require(ggplot2)
library(plyr)
library(reshape)
library(data.table)
library(xlsx)

setwd("C:/Users/hernan/Desktop/auth0/ExerciseHernan")

getwd()

#start importing data from cvs source.

logs = read.csv("Logs.csv")

logs = logs[,c(2,1,3,4)]

logs$at = as.Date(logs$at)

# The next thing to do is define the type of the event, and filter unused

logs = subset(logs, !is.na(logs$at))

logs$type <- ifelse((logs$event == "ss"), "Signups",ifelse((logs$event == "s"), "Logins",ifelse((logs$event == "fs"), "Failed Signups",ifelse((logs$event %in% c('f', 'fc', 'fp', 'fu', 'fco')),"Failed Logins","Unused"))))
logs <- subset(logs, logs$type!="Unused")

logs <- logs[,c(1,2,3,5)]

logs$at <-as.Date(cut(logs$at,breaks = "month"))

#TOTAL VALUE:

TotalValue = count(logs, c('at','type'))

#AVERAGE BY TENANT:

tenants = count(logs, c('at','tenant','type'))
tenantsAvg = aggregate(freq ~ at+type,tenants, mean)
tenantsAvg <- cast(tenantsAvg, at ~ type)
tenantsAvg <- rename(tenantsAvg, c("Failed Logins"="FailLogins AvgTenant",
                                   "Logins"="Logins AvgTenant", "Signups"="Signups AvgTenant", "Failed Signups"="FailedSignups AvgTenant"))

#PERCENTILE 80%

Percentile = 80

rankingTable <- as.data.table(tenants)
rankingTable[,freqRank:=rank(+freq,ties.method="first"),by=c('at','type')]

percentile80 = aggregate(freqRank~at+type, rankingTable, max)
percentile80$freqRank = floor((percentile80$freqRank+1)*Percentile/100)

rankingTable <- merge(rankingTable, percentile80, by= c('at','type','freqRank'))
rankingTable <- cast(rankingTable, at ~ type)
rankingTable <- rename(rankingTable, c("Failed Logins"="FailLogins 80 Perct",
                                       "Logins"="Logins 80 Perct", "Signups"="Signups 80 Perct", "Failed Signups"="FailedSignups 80 Perct"))

#AVERAGE BY TENANT+APP

AverageTenantApp = tenants = count(logs, c('at','type','tenant','app'))
AverageTenantApp = aggregate(freq ~ at+type,tenants, mean)
AverageTenantApp <- cast(AverageTenantApp, at ~ type)
AverageTenantApp <- rename(AverageTenantApp, c("Failed Logins"="FailLogins AvgTenantApp",
                                               "Logins"="Logins AvgTenantApp", "Signups"="Signups AvgTenantApp", "Failed Signups"="FailedSignups AvgTenantApp"))

#DIFFERENCE WITH PREVIOUS MONTH:

TotalValuePrev = TotalValue
TotalValuePrev$atPrev = TotalValuePrev$at + 31
TotalValuePrev$atPrev = as.Date(cut(TotalValuePrev$atPrev,breaks = "month"))
TotalValuePrev <- TotalValuePrev[c(4,1,2,3)]
TotalValuePrev <- rename(TotalValuePrev, c("atPrev"="at", "at"="atPrev"))

diffTotalValues = merge(x = TotalValue,y = TotalValuePrev,by=c("at","type"), all.x = TRUE)
diffTotalValues$diff = diffTotalValues$freq.x - diffTotalValues$freq.y
diffTotalValues = diffTotalValues[,c(1,2,6)]

diffTotalValues <- cast(diffTotalValues, at ~ type)
diffTotalValues <- rename(diffTotalValues, c("Failed Logins"="FailLogins Diff",
                                             "Logins"="Logins Diff", "Signups"="Signups Diff", "Failed Signups"="FailedSignups Diff"))
TotalValue <- cast(TotalValue, at ~ type)
TotalValue <- rename(TotalValue, c("Failed Logins"="FailLogins Total",
                                   "Logins"="Logins Total", "Signups"="Signups Total", "Failed Signups"="FailedSignups Total"))

#% SUCcESSFUL LOGGINS & SIGNUPS:

Successfuls = TotalValue
Successfuls$"Logins Total" = replace(Successfuls$"Logins Total", is.na(Successfuls$"Logins Total"),0)
Successfuls$"Signups Total" = replace(Successfuls$"Signups Total", is.na(Successfuls$"Signups Total"),0)
Successfuls$"FailLogins Total" = replace(Successfuls$"FailLogins Total", is.na(Successfuls$"FailLogins Total"),0)
Successfuls$"FailedSignups Total" = replace(Successfuls$"FailedSignups Total", is.na(Successfuls$"FailedSignups Total"),0)

Successfuls$'% Successfuls Loggins' = TotalValue$'Logins Total'/(TotalValue$'Logins Total' + TotalValue$'FailLogins Total')*100
Successfuls$'% Successfuls Signups' = TotalValue$'Signups Total'/(TotalValue$'Signups Total' + TotalValue$'FailedSignups Total')*100

Successfuls = Successfuls[,c(1,6,7)]

#Generating the chart (Total Events by Type)

ggplot(TotalValue, aes(TotalValue$at)) + 
  geom_line(aes(y = TotalValue$'Logins Total', colour = "Logins Total")) + 
  geom_line(aes(y = TotalValue$'Signups Total', colour = "Signups Total")) +
  geom_line(aes(y = TotalValue$'FailLogins Total', colour = "FailLogins Total")) + 
  geom_line(aes(y = TotalValue$'FailedSignups Total', colour = "FailedSignups Total")) +
  xlab("Dates") + ylab ("Events") + ggtitle("Total Events by Month ")

#Assembling the final report (order columns, and export to XLSX)

Events_Log_Report = merge(TotalValue, tenantsAvg, by=c('at'), x.ALL=TRUE)
Events_Log_Report = merge(Events_Log_Report, AverageTenantApp, by=c('at'), x.ALL=TRUE)
Events_Log_Report = merge(Events_Log_Report, rankingTable, by=c('at'), x.ALL=TRUE)
Events_Log_Report = merge(Events_Log_Report, diffTotalValues, by=c('at'), x.ALL=TRUE)
Events_Log_Report = merge(Events_Log_Report, Successfuls, by=c('at'), x.ALL=TRUE)
Events_Log_Report = Events_Log_Report[,c(1,2,6,10,14,18,3,7,11,15,19,4,8,12,16,20,5,9,13,17,21,22,23)]
write.xlsx(Events_Log_Report, "Events Log Report.xlsx", sheetName="Events Log")